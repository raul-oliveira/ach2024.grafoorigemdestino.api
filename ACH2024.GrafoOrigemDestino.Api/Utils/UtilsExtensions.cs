﻿using ACH2024.GrafoOrigemDestino.Api.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;

namespace ACH2024.GrafoOrigemDestino.Api.Utils
{
    public static class UtilsExtensions
    {
        private static CultureInfo CulturePtBr { get => CultureInfo.GetCultureInfo("pt-BR"); }

        private static IConfiguration configuration { get => Program.Configuration; }

        public static int GetInt(this string[] array, int index)
        {
            ValidarArrayEIndex(array, index - 1);
            return int.Parse(array[index - 1], CulturePtBr);
        }

        public static long GetLong(this string[] array, int index)
        {
            ValidarArrayEIndex(array, index - 1);
            return long.Parse(array[index - 1], CulturePtBr);
        }

        public static bool GetBool(this string[] array, int index)
        {
            ValidarArrayEIndex(array, index - 1);
            return array[index - 1] == "1";
        }

        public static double GetDouble(this string[] array, int index)
        {
            ValidarArrayEIndex(array, index - 1);
            return double.Parse(array[index - 1], CulturePtBr);
        }

        public static void OpenIndexHtml()
        {
            var folder = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            OpenBrowser(string.Concat(folder, "\\index.html"));
        }

        public static DateTime GetDate(this string[] array, int index)
        {
            ValidarArrayEIndex(array, index - 1);
            var date = array[index - 1].PadLeft(8, '0');
            var format = "ddMMyyyy";
            if (DateTime.TryParseExact(
                date,
                format,
                CultureInfo.InvariantCulture,
                DateTimeStyles.None,
                out var parsedDate))
            {
                return parsedDate;
            }
            return DateTime.MinValue;
        }

        public static void OpenBrowser(string url)
        {
            try
            {
                Process.Start(url);
            }
            catch
            {
                // hack because of this: https://github.com/dotnet/corefx/issues/10361
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                {
                    url = url.Replace("&", "^&");
                    Process.Start(new ProcessStartInfo("cmd", $"/c start {url}") { CreateNoWindow = true });
                }
                else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                {
                    Process.Start("xdg-open", url);
                }
                else if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
                {
                    Process.Start("open", url);
                }
                else
                {
                    throw;
                }
            }
        }

        private static void ValidarArrayEIndex(this string[] array, int index)
        {
            if (array == null || string.IsNullOrEmpty(array[index]))
                throw new ArgumentNullException(nameof(array));
        }

        internal static bool MontarGrafico(List<string> legendas, List<double> valores)
        {
            var folder = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);

            var chartjs = new ChartjsDataConfig()
            {
                data = new List<ChartjsData> {
                    new ChartjsData()
                    {
                        backgroundColor = configuration["chartjsDataConfig:backgroundColor"],
                        borderColor = configuration["chartjsDataConfig:borderColor"],
                        data = valores,
                        label = configuration["chartjsDataConfig:label"]
                    }
                },
                labels = legendas
            };


            var chartjsConfig = File.ReadAllText(string.Concat(folder, "\\chartconfigTemplate.js")).Replace("{$ChartDataConfig}", JsonConvert.SerializeObject(chartjs, Formatting.Indented));
            File.WriteAllText(string.Concat(folder, "\\chartconfig.js"), chartjsConfig);
            return true;
        }
    }
}
