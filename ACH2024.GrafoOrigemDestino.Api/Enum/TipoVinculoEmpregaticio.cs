﻿namespace ACH2024.GrafoOrigemDestino.Api.Enum
{
    public enum TipoVinculoEmpregaticio
    {
        AssalariadoComCarteira = 1,
        AssalariadoSemCarteira,
        FuncionarioPublicol,
        Autonomo,
        Empregador,
        ProfissionalLiberal,
        DonosDeNegocioFamiliar,
        TrabalhoFamiliar
    }
}
