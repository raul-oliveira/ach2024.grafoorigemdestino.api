﻿namespace ACH2024.GrafoOrigemDestino.Api.Enum
{
    public enum TipoSituacaoEstudo
    {
        Nao = 1,
        Creche_PreEscola,
        PrimerioGrau_Fundamental,
        SegundoGrau_Medio,
        Superior_Universitario,
        Outros
    }
}
