﻿namespace ACH2024.GrafoOrigemDestino.Api.Enum
{
    public enum TipoEntrevista
    {
        CompletaComViagem = 1,
        CompletaSemViagem = 2
    }
}
