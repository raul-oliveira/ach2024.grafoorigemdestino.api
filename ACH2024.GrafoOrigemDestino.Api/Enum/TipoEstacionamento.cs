﻿namespace ACH2024.GrafoOrigemDestino.Api.Enum
{
    public enum TipoEstacionamento
    {
        NaoEstacionou = 1,
        ZonaAzul,
        Patrocinado,
        Proprio,
        MeioFio,
        Avulso,
        Mensal,
        E_Facil,
        NaoRespondeu
    }
}
