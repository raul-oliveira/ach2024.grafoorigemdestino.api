﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ACH2024.GrafoOrigemDestino.Api.Enum
{
    public enum TipoDiaDaSemana
    {
        Segunda = 2,
        Terca,
        Quarta,
        Quinta,
        Sexta
    }
}
