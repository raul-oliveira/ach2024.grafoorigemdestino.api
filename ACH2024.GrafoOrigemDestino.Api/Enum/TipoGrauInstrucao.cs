﻿namespace ACH2024.GrafoOrigemDestino.Api.Enum
{
    public enum TipoGrauInstrucao
    {
        NaoAlfabetizado_Fundamental_I_Incompleto = 1,
        Fundamento_I_Completo_Fundamental_II_Incompleto,
        MedioCompleto_SuperiorIncompleto,
        SuperiorCompleto
    }
}
