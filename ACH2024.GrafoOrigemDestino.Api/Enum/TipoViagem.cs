﻿namespace ACH2024.GrafoOrigemDestino.Api.Enum
{
    public enum TipoViagem
    {
        Coletivo = 1,
        Individual,
        A_Pe,
        Bicicleta
    }
}
