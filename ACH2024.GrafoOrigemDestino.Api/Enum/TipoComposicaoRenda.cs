﻿namespace ACH2024.GrafoOrigemDestino.Api.Enum
{
    public enum TipoComposicaoRenda
    {
        TemRenda = 1,
        NaoTemRenda,
        NaoRespondeu
    }
}
