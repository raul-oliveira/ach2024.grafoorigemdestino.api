﻿namespace ACH2024.GrafoOrigemDestino.Api.Enum
{
    public enum TipoMotivoViagem
    {
        PequenaDistancia = 1,
        ConduncaoCara, 
        Ponto_Estacao_Distante,
        ConducaoDemoraPraPassar,
        ViagemDemorada,
        ConducaoLotada,
        AtividadeFisica,
        OutrosMotivos
    }
}
