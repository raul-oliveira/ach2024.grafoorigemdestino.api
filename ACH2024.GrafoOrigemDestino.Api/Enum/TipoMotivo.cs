﻿namespace ACH2024.GrafoOrigemDestino.Api.Enum
{
    public enum TipoMotivo
    {
        TrabalhoIndustria = 1,
        TrabalhoComercio,
        TrabalhoServicos,
        Escola_Educacao,
        Compras,
        Medico_Dentista_Saude,
        Recreacao_Visitas_Lazer,
        Residencia,
        ProcurarEmprego,
        AssuntosPessoais,
        Refeicao
    }
}
