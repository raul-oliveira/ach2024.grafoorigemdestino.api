﻿namespace ACH2024.GrafoOrigemDestino.Api.Enum
{
    public enum TipoPagador
    {
        Voce_SuaFamilia = 1,
        Empregador,
        Isento,
        Outros
    }
}
