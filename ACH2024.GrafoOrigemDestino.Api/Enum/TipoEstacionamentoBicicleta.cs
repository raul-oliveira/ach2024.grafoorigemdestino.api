﻿namespace ACH2024.GrafoOrigemDestino.Api.Enum
{
    public enum TipoEstacionamentoBicicleta
    {
        BicicletarioGratuito = 1,
        BicicletarioPago,
        LocalPrivado,
        Rua_Local_Publico,
        GuardadorDeRua,
        EstacaoDeBicicleta,
        ParacicloPublico,
        Outros
    }
}
