﻿namespace ACH2024.GrafoOrigemDestino.Api.Enum
{
    public enum TipoOcupacao
    {
        MembrosSuperioresPoderPublico_DirigentesDeOrganizacoes_Gerentes = 1,
        ProfissionaisDasCienciasEDasArtes,
        TecnicosDeNivelMedio,
        TrabalhadoresDeServicosAdministrativos,
        TrabalhadoresDosServicos,
        VendedoresDoComercioEmLojasEMercados,
        TrabalhadoresAgropecuarios_Florestais_Pesca,
        TrabalhadoresDaProducaoDeBensEServicosIndustriais,
        TrabalhadoresEmServicosDeReparacaoEManuntencao,
        MembrosDasForcasArmadas_Policiais_BombeirosMilitares
    }
}
