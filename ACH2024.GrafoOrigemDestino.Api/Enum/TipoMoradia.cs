﻿namespace ACH2024.GrafoOrigemDestino.Api.Enum
{
    public enum TipoMoradia
    {
        Alugada = 1,
        Propria,
        Cedida,
        Outros,
        NaoRepondeu
    }
}
