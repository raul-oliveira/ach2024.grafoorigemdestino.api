﻿namespace ACH2024.GrafoOrigemDestino.Api.Enum
{
    public enum TipoRendaFamiliar
    {
        RendaFamiliarDeclaradaEMaiorQueZero = 1,
        RendaFamiliarDeclaradaComoZero,
        RendaAtribuidaPeloCriterioBrasil,
        RendaAtribuidaPelaMediaDaZona
    }
}
