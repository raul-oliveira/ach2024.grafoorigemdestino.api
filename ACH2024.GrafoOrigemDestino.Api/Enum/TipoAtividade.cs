﻿namespace ACH2024.GrafoOrigemDestino.Api.Enum
{
    public enum TipoAtividade
    {
        TemTrabalhoRegular = 1,
        FazBico,
        EmLicencaMedica,
        Aprosentado_Pensionista,
        SemTrabalho,
        NuncaTrabalhou,
        DonaDecasa,
        Estudante
    }
}
