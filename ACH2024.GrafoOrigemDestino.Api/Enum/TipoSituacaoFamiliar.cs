﻿namespace ACH2024.GrafoOrigemDestino.Api.Enum
{
    public enum TipoSituacaoFamiliar
    {
        PessoaResponsavel = 1,
        Conjuge_Companheiro,
        Filho_Enteado,
        OutroParente,
        Agregado,
        EmpregadoResidente,
        ParenteDoEmpregadoResidente
    }
}
