﻿namespace ACH2024.GrafoOrigemDestino.Api.Enum
{
    public enum TipoModoViagem
    {
        Metro = 1,
        Trem,
        Monotrilho,
        Onibus_MicroOnibus_Perua_Municipal,
        Onibus_MicroOnibus_Perua_OutrosMunicipios,
        Onibus_MicroOnibus_Perua_Metropolitano,
        Fretado,
        Escolar,
        DirigindoAutomovel,
        PassageiroAutomovel,
        TaxiConvencional,
        TaxiNaoConvencional,
        DirigindoMoto,
        PassageiroMoto,
        Bicicleta,
        A_Pe, //A pé
        Outros
    }
}
