﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ACH2024.GrafoOrigemDestino.Api.Enum
{
    public enum TipoSetorAtividade
    {
        Agricola = 1,
        ConstrucaoCivil,
        Industria,
        Comercio,
        ServicoDeTransporteDeCarga,
        ServicoDeTransporteDePassageiros,
        ServicoCrediticio_Financeiro,
        ServicoPessoal,
        ServicoDeAlimentacao,
        ServicoDeSaude,
        ServicoDeEducacao,
        ServicoEspecializado,
        ServicoDeAdministracaoPublica,
        OutrosServicos
    }
}
