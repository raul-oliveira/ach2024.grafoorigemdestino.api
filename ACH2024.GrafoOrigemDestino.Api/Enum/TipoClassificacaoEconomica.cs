﻿namespace ACH2024.GrafoOrigemDestino.Api.Enum
{
    public enum TipoClassificacaoEconomica
    {
        A = 1,
        B1,
        B2,
        C1,
        C2,
        D_E
    }
}
