﻿namespace ACH2024.GrafoOrigemDestino.Api.Enum
{
    public enum TipoTrabalhoIgualResidencia
    {
        Sim = 1,
        Nao,
        SemEnderecoFixo
    }
}
