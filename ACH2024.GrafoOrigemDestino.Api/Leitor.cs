﻿using ACH2024.GrafoOrigemDestino.Api.Models;
using ACH2024.GrafoOrigemDestino.Api.Utils;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ACH2024.GrafoOrigemDestino.Api
{
    public class Leitor
    {
        public Leitor()
        {
            Linhas = new List<Linha>();
            _configuration = Program.Configuration;
        }

        #region Propriedades
        private readonly IConfiguration _configuration;

        private IEnumerable<string> _linhasTexto { get; set; }

        public List<Linha> Linhas { get; private set; }

        public List<Pessoa> Pessoas { get; private set; }

        public List<Viagem> Viagens { get; private set; }

        public List<int> Familias { get; private set; }

        /// <summary>
        /// Controle de visitados para busca em profundidade recursiva
        /// </summary>
        private List<long> Visitados { get; set; }
        
        /// <summary>
        /// Controle de visitados para busca em profundidade iterativa
        /// </summary>
        public List<Pessoa> VisitadosBPI { get; set; }
        #endregion

        #region Métodos

        public List<Linha> ProcessarArquivo()
        {
            Linhas = ConverterParaLinhas(LerAquivo());
            return Linhas;
        }

        public List<Local> CalcularFrequentadoresDosLocais()
        {
            var locais = new List<Local>();
            Parallel.ForEach(Viagens, viagem =>
            {
                var origem = new Local();
                var destino = new Local();
                lock (locais)
                {
                    origem = locais.FirstOrDefault(x => x.CoordenadaX == viagem.Origem.CoordenadaX && x.CoordenadaY == viagem.Origem.CoordenadaY);
                    destino = locais.FirstOrDefault(x => x.CoordenadaX == viagem.Destino.CoordenadaX && x.CoordenadaY == viagem.Destino.CoordenadaY);
                }

                if (origem != null)
                {
                    lock (locais)
                    {
                        if (!origem.Frequentadores.Any(x => x == viagem.IdPessoa))
                        {
                            origem.Frequentadores.Add(viagem.IdPessoa);
                        }
                    }
                }
                else
                {
                    origem = viagem.Origem;
                    origem.Frequentadores.Add(viagem.IdPessoa);

                    if (origem.CoordenadaX != 0 && origem.CoordenadaY != 0)
                    {
                        lock (locais)
                        {
                            locais.Add(origem);
                        }
                    }
                }

                if (destino != null)
                {
                    lock (locais)
                    {
                        if (!destino.Frequentadores.Any(x => x == viagem.IdPessoa))
                        {
                            destino.Frequentadores.Add(viagem.IdPessoa);
                        }
                    }
                }
                else
                {
                    destino = viagem.Destino;
                    destino.Frequentadores.Add(viagem.IdPessoa);

                    if (destino.CoordenadaX != 0 && destino.CoordenadaY != 0)
                    {
                        lock (locais)
                        {
                            locais.Add(viagem.Destino);
                        }
                    }
                }

            });
            var locaisjson = JsonConvert.SerializeObject(locais);

            var folder = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            File.WriteAllText(string.Concat(folder, "\\locais.txt"), locaisjson, Encoding.UTF8);
            return locais;
        }

        public void GerarHistograma(List<Local> locais = null)
        {
            var dic = new Dictionary<int, long>();
            var folder = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);

            if (locais == null)
            {
                locais = JsonConvert.DeserializeObject<List<Local>>(File.ReadAllText(string.Concat(folder, "\\locais.txt")));
            }

            var group = locais.GroupBy(x => x.TotalFrequentadores).OrderBy(x => x.Key).ToList();
            var oitoMais = group.Where(x => x.Key > 8).Sum(x => x.Count());

            var legendas = group.Select(x => x.Key < 8 ? $"{x.Key.ToString()} visitante(s)" : "8+  visitante(s)").Distinct().ToList();
            var valores = group.Where(x => x.Key <= 8)
                                .Select(x => x.Key == 8 ? Convert.ToDouble(x.Count()) + oitoMais : Convert.ToDouble(x.Count()))
                                .ToList();

            UtilsExtensions.MontarGrafico(legendas, valores);
            UtilsExtensions.OpenIndexHtml();
        }

        public List<Pessoa> ObterPessoas()
        {
            Pessoas = Linhas.Where(x => x.EhPrimeiroRegistroPessoa).Select(x => new Pessoa(x) { }).ToList();
            return Pessoas;
        }

        public List<int> ObterFamilias()
        {
            Familias = Linhas.Select(x => x.IdFamilia).Distinct().ToList();
            return Familias;
        }

        public List<Viagem> ObterViagens()
        {
            Viagens = Linhas.Select(x => new Viagem(x) { })
                .Where(x => x.Origem.CoordenadaX != 0 && x.Origem.CoordenadaY != 0 && x.Destino.CoordenadaY != 0 && x.Destino.CoordenadaX != 0)
                .ToList();
            return Viagens;
        }

        private IEnumerable<string> LerAquivo()
        {
            var folder = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            try
            {
                this._linhasTexto = File.ReadAllLines(string.Concat(folder, "\\OD_2017.csv"), Encoding.UTF8);
            }
            catch (Exception)
            {
                this._linhasTexto = File.ReadAllLines(@"C:\Temp\OD_2017.csv", Encoding.UTF8);
            }

            return _linhasTexto;
        }

        private static List<Linha> ConverterParaLinhas(IEnumerable<string> input)
        {
            var resultado = new List<Linha>();
            Parallel.ForEach(input, new ParallelOptions() { MaxDegreeOfParallelism = 16 }, (stringLinha, state, index) =>
            {
                if (stringLinha[0] != 'Z')
                {
                    var linha = new Linha(stringLinha);
                    lock (resultado)
                    {
                        resultado.Add(linha);
                    }
                }
            });
            return resultado;
        }

        public List<Pessoa> PreencherConexoes(List<Pessoa> pessoas)
        {
            var folder = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            List<Local> locais = JsonConvert.DeserializeObject<List<Local>>(File.ReadAllText(string.Concat(folder, "\\locais.txt")));

            Parallel.ForEach(pessoas, new ParallelOptions { MaxDegreeOfParallelism = 16 }, pessoa =>
             {
                 var locaisVisitados = locais.FindAll(local => local.Frequentadores.Contains(pessoa.IdPessoa));

                 foreach (var local in locaisVisitados)
                 {
                     var pessoasQueEstaoNoMesmoLocal = local.Frequentadores.Where(idAtual => !pessoa.Conexoes.Contains(idAtual) && idAtual != pessoa.IdPessoa);
                     pessoa.Conexoes.AddRange(pessoasQueEstaoNoMesmoLocal);
                 }
             });

            return pessoas;
        }

       /// <summary>
       /// versão recursiva
       /// </summary>
       /// <param name="grafo"></param>
        public void ObterComponentesConexos(List<Pessoa> grafo)
        {
            Visitados = new List<long>();

            var aglomeracoes = new List<List<long>>();
            for (int i = 0; i < grafo.Count; i++)
            {
                aglomeracoes.Add(new List<long>());
            }
            for (int i = 0; i < grafo.Count; i++)
            {
                BuscaProfundidade(grafo[i], aglomeracoes[i]);
            }
            aglomeracoes.RemoveAll(x => x.Count == 0);
        }

        /// <summary>
        /// versão recursiva
        /// </summary>
        /// <param name="no"></param>
        /// <param name="aglomeracao"></param>
        private void BuscaProfundidade(Pessoa no, List<long> aglomeracao)
        {
            if (Visitados.Any(x => x == no.IdPessoa))
                return;

            Visitados.Add(no.IdPessoa);

            if (no.TotalConexoes == 0)
            {
                aglomeracao.Add(no.IdPessoa);
                return;
            }
            aglomeracao.Add(no.IdPessoa);

            foreach (var conexao in no.Conexoes)
            {
                var objPessoa = Pessoas.FirstOrDefault(x => x.IdPessoa == conexao);
                BuscaProfundidade(objPessoa, aglomeracao);
            }
        }

        public void BuscaProfundidadeIterativa(Pessoa pessoaInicial, List<long> aglomeracao)
        {
            var faltaVisitar = new Stack<Pessoa>();
            faltaVisitar.Push(pessoaInicial);

            while (faltaVisitar.Count != 0)
            {
                Pessoa vertice = faltaVisitar.Pop();
                lock (VisitadosBPI)
                {
                    if (!VisitadosBPI.Any(x => x.IdPessoa == vertice.IdPessoa))
                    {
                        VisitadosBPI.Add(vertice);
                    }
                }

                if (vertice.TotalConexoes == 0)
                {
                    aglomeracao.Add(vertice.IdPessoa);
                }
                else
                {
                    aglomeracao.Add(vertice.IdPessoa);
                    foreach (long vizinho in vertice.Conexoes)
                    {
                        var objPessoa = Pessoas.FirstOrDefault(x => x.IdPessoa == vizinho);
                        lock (VisitadosBPI)
                        {
                            if (!VisitadosBPI.Contains(objPessoa))
                            {
                                VisitadosBPI.Add(objPessoa);
                                faltaVisitar.Push(objPessoa);
                            }
                        }
                    }
                }

            }
            Console.WriteLine("Total de nós visitados: {0}", VisitadosBPI.Count);
        }

        public List<List<long>> ObterComponentesConexosIterativo(List<Pessoa> grafo)
        {
            int nroGrafosConexos = 0;
            VisitadosBPI = new List<Pessoa>();

            var aglomeracoes = IniciarAglomeracoes(grafo.Count);

            // foreach paralelo para fazer a busca em profundidade interativa
            Parallel.ForEach(grafo, new ParallelOptions { MaxDegreeOfParallelism = 16 }, (pessoa, state, index) =>
             {
                 bool visitado = true;
                 lock (VisitadosBPI)
                 {
                     visitado = VisitadosBPI.Any(x => x.IdPessoa == pessoa.IdPessoa);
                 }
                 if (!visitado)
                 {
                     Console.WriteLine("Grafo conexo nro : {0}", nroGrafosConexos++);
                     BuscaProfundidadeIterativa(pessoa, aglomeracoes[(int)index]);
                 }
             });

            RemoverTodasAglomeracoesVazias(aglomeracoes);
            
            // ordena da maior aglomeração para a menor
            return aglomeracoes.OrderByDescending(x => x.Count).ToList();
        }

        private List<List<long>> IniciarAglomeracoes (int quantidade)
        {
            var aglomeracoes = new List<List<long>>();
            for (int i = 0; i < quantidade; i++)
            {
                aglomeracoes.Add(new List<long>());
            }
            return aglomeracoes;
        }

        private void RemoverTodasAglomeracoesVazias(List<List<long>> aglomeracoes)
        {
            aglomeracoes.RemoveAll(x => x.Count == 0);
        }

        #endregion

        #region Métodos que precisam ser revisados
        public void ContarArestas(List<Pessoa> grafo)
        {
            var contados = new List<long>();
            foreach (var no in grafo)
            {
                var arestasElegiveis = no.Conexoes.Where(x => !contados.Contains(x));
                contados.AddRange(arestasElegiveis);
            }
        }

        public void GerarDistribuicao(List<Pessoa> grafo)
        {
            var dic = new Dictionary<int, int>();

            var group = grafo.GroupBy(x => x.TotalConexoes).OrderBy(x => x.Key).ToList();
        }
        #endregion
    }
}
