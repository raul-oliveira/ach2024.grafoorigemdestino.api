﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace ACH2024.GrafoOrigemDestino.Api
{
    public class Program
    {
        public static IConfigurationRoot Configuration { get; set; }
        
        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            Configuration = builder.Build();

            var leitor = new Leitor();
            DateTime start, end;
            var linhas = 0;

            Console.WriteLine($"Inicou: {nameof(leitor.ProcessarArquivo)} - {(start = DateTime.Now).ToString("HH:mm:ss")}");
            linhas = leitor.ProcessarArquivo().Count;
            Console.WriteLine($"{linhas} linhas foram encontradas - {DateTime.Now.ToString("HH:mm:ss")}");
            Console.WriteLine($"Finalizou: {nameof(leitor.ProcessarArquivo)} - {(end = DateTime.Now).ToString("HH:mm:ss")}");
            Console.WriteLine($"Tempo gasto - {(end - start).TotalSeconds}");
            Console.WriteLine("===================================================");

            Console.WriteLine($"Inicou: {nameof(leitor.PreencherConexoes)} - {(start = DateTime.Now).ToString("HH:mm:ss")}");
            leitor.PreencherConexoes(leitor.ObterPessoas());
            Console.WriteLine($"Finalizou: {nameof(leitor.PreencherConexoes)} - {(end = DateTime.Now).ToString("HH:mm:ss")}");
            Console.WriteLine($"Tempo gasto - {(end - start).TotalSeconds}");
            Console.WriteLine("===================================================");

            leitor.ObterComponentesConexosIterativo(leitor.Pessoas);
        }
    }
}
