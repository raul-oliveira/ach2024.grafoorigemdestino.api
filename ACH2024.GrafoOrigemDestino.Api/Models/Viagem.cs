﻿using System;
using System.Collections.Generic;

namespace ACH2024.GrafoOrigemDestino.Api.Models
{
    public class Viagem
    {
        public Viagem(Linha linha)
        {
            IdPessoa = linha.IdPessoa;
            MotivoOrigem = linha.MotivoNaOrigem;
            MotivoDestino = linha.MotivoNoDestino;
            DataHoraSaida = linha.DataDaEntrevista.AddHours(linha.HoraSaida).AddMinutes(linha.MinutoSaida);
            DataHoraChegada = linha.DataDaEntrevista.AddHours(linha.HoraChegada).AddMinutes(linha.MinutoChegada);
            Origem = new Local()
            {
                CoordenadaX = linha.CoordenadaXOrigem,
                CoordenadaY = linha.CoordenadaYOrigem,
                Municipio = linha.MunicipioOrigem,
                Zona = linha.ZonaDeOrigem
            };
            Destino = new Local()
            {
                CoordenadaX = linha.CoordenadaXDestino,
                CoordenadaY = linha.CoordenadaYDestino,
                Municipio = linha.MunicipioDeDestino,
                Zona = linha.ZonaDeDestino
            };
            Transferencias = new List<Local>();
            Transferencias.Add(new Transferencia()
            {
                Ordem = 1,
                CoordenadaX = linha.CoordenadaXPrimeiraTransferencia,
                CoordenadaY = linha.CoordenadaYPrimeiraTransferencia,
                Municipio = linha.MunicipioPrimeiraTransferencia,
                Zona = linha.ZonaPrimeiraTransferencia
            });
            Transferencias.Add(new Transferencia()
            {
                Ordem = 2,
                CoordenadaX = linha.CoordenadaXSegundaTransferencia,
                CoordenadaY = linha.CoordenadaYSegundaTransferencia,
                Municipio = linha.MunicipioSegundaTransferencia,
                Zona = linha.ZonaSegundaTransferencia
            });
            Transferencias.Add(new Transferencia()
            {
                Ordem = 3,
                CoordenadaX = linha.CoordenadaXTerceiraTransferencia,
                CoordenadaY = linha.CoordenadaYTerceiraTransferencia,
                Municipio = linha.MunicipioTerceiraTransferencia,
                Zona = linha.ZonaTerceiraTransferencia
            });
        }

        public Viagem() { }

        public long IdPessoa { get; set; }

        public DateTime DataHoraSaida { get; set; }

        public DateTime DataHoraChegada { get; set; }

        public Enum.TipoMotivo MotivoOrigem { get; set; }

        public Enum.TipoMotivo MotivoDestino { get; set; }

        public Local Origem { get; set; }

        public Local Destino { get; set; }

        public List<Local> Transferencias { get; set; }

    }
}
