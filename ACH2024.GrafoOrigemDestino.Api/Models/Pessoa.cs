﻿using System.Collections.Generic;

namespace ACH2024.GrafoOrigemDestino.Api.Models
{
    public class Pessoa
    {
        public Pessoa(Linha linha)
        {
            IdPessoa = linha.IdPessoa;
            IdFamilia = linha.IdFamilia;
            Idade = linha.Idade;
            Sexo = linha.Genero;
            Conexoes = new List<long>();
        }

        public Pessoa() { }
        
        public long IdPessoa { get; set; }

        public long IdFamilia { get; set; }
        
        public int Idade { get; set; }

        public Enum.TipoGenero Sexo { get; set; }

        public List<long> Conexoes { get; set; }
        
        public int TotalConexoes { get => Conexoes.Count; }
    }
}
