﻿using System.Collections.Generic;

namespace ACH2024.GrafoOrigemDestino.Api.Models
{
    public class ChartjsDataConfig
    {
        public List<string> labels { get; set; }
        public List<ChartjsData> data { get; set; }
    }

    public class ChartjsData
    {
        public string label { get; set; }
        public List<double> data { get; set; }
        public string backgroundColor { get; set; }
        public string borderColor { get; set; }
    }
}
