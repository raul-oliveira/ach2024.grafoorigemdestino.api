﻿using ACH2024.GrafoOrigemDestino.Api.Enum;
using ACH2024.GrafoOrigemDestino.Api.Utils;
using System;

namespace ACH2024.GrafoOrigemDestino.Api.Models
{
    public class Linha
    {
        public Linha(string dados)
        {
            var props = dados.Split(';');
            ZonaDoDomicilio = props.GetInt(ZONA);
            MunicipioDomicilio = props.GetInt(MUNI_DOM);
            CoordenadaXDomicilio = props.GetInt(CO_DOM_X);
            CoordenadaYDomicilio = props.GetInt(CO_DOM_Y);
            IdDomicilio = props.GetInt(ID_DOM);
            EhPrimeiroRegistroDomicilio = props.GetBool(F_DOM);
            FatorDeExpansaoDoDomicilio = props.GetDouble(FE_DOM);
            NumeroDomicilio = props.GetInt(DOM);
            CodEntrevista = (TipoEntrevista)props.GetInt(CD_ENTRE);
            DataDaEntrevista = props.GetDate(DATA);
            TipoDomicilio = (Enum.TipoDomicilio)props.GetInt(TIPO_DOM);
            PossuiAguaEncanada = props.GetBool(AGUA);
            RuaPavimentada = props.GetBool(RUA_PAVI);
            TotalDeMoradoresNoDomicilio = props.GetInt(NO_MORAD);
            TotalDeFamiliasNoDomicilio = props.GetInt(TOT_FAM);
            IdFamilia = props.GetInt(ID_FAM);
            EhPrimeiroRegistroFamilia = props.GetBool(F_FAM);
            FatorDeExpansaoDaFamilia = props.GetDouble(FE_FAM);
            NumeroDaFamilia = props.GetInt(FAMILIA);
            TotalDeMoradoresNaFamilia = props.GetInt(NO_MORAF);
            CondicaoMoradia = (TipoMoradia)props.GetInt(CONDMORA);

            QtdBanheiros = props.GetInt(QT_BANHO);
            QtdEmpregadosDomesticos = props.GetInt(QT_EMPRE);
            QtdAutomoveis = props.GetInt(QT_AUTO);
            QtdMicrocomputadores = props.GetInt(QT_MICRO);
            QtdMaquinasDeLavarLouca = props.GetInt(QT_LAVALOU);
            QtdGeladeirasDe1Porta = props.GetInt(QT_GEL1);
            QtdGeladeirasDe2Portas = props.GetInt(QT_GEL2);
            QtdFreezer = props.GetInt(QT_FREEZ);
            QtdMaquinasDeLavar = props.GetInt(QT_MLAVA);
            QtdDvds = props.GetInt(QT_DVD);
            QtdMicroondas = props.GetInt(QT_MICROON);
            QtdMotos = props.GetInt(QT_MOTO);
            QtdSecadoraDeRoupas = props.GetInt(QT_SECAROU);
            QtdBicicletas = props.GetInt(QT_BICICLE);

            DeclarouItensDeConforto = props.GetBool(NAO_DCL_IT);
            CriterioDeClassificacaoEconomica = (TipoClassificacaoEconomica)props.GetInt(CRITERIOBR);
            PontosCriterioBrasil = props.GetInt(PONTO_BR);
            AnoFabricacaoAuto1 = props.GetInt(ANO_AUTO1);
            AnoFabricacaoAuto2 = props.GetInt(ANO_AUTO2);
            AnoFabricacaoAuto3 = props.GetInt(ANO_AUTO3);

            RendaFamiliarMensal = props.GetDouble(RENDA_FA);
            CodigoDeRendaFamiliar = (TipoRendaFamiliar)props.GetInt(CD_RENFA);
            IdPessoa = props.GetLong(ID_PESS);
            EhPrimeiroRegistroPessoa = props.GetBool(F_PESS);
            FatorExpansaoPessoa = props.GetDouble(FE_PESS);
            NumeroDaPessoa = props.GetInt(PESSOA);
            SituacaoFamiliar = (TipoSituacaoFamiliar)props.GetInt(SIT_FAM);
            Idade = props.GetInt(IDADE);
            Genero = (TipoGenero)props.GetInt(SEXO);
            EstudaAtualmente = (TipoSituacaoEstudo)props.GetInt(ESTUDA);
            GrauInstrucao = (TipoGrauInstrucao)props.GetInt(GRAU_INS);
            CondicaoDeAtividade = (TipoAtividade)props.GetInt(CD_ATIVI);
            CondicaoDeRendaIndividual = (TipoComposicaoRenda)props.GetInt(CO_REN_I);
            RendaIndividual = props.GetDouble(VL_REN_I);
            ZonaDaEscola = props.GetInt(ZONA_ESC);
            MunicipioDaEscola = props.GetInt(MUNIESC);
            CoordenadaXEscola = props.GetInt(CO_ESC_X);
            CoordenadaYEscola = props.GetInt(CO_ESC_Y);
            TipoEscola = (TipoEscola)props.GetInt(TIPO_ESC);
            ZonaPrimeiroTrabalho = props.GetInt(ZONATRA1);
            MunicipioPrimeiroTrabalho = props.GetInt(MUNITRA1);
            CoordenadaXPrimeiroTrabalho = props.GetInt(CO_TR1_X);
            CoordenadaYPrimeiroTrabalho = props.GetInt(CO_TR1_Y);
            PrimeiroTrabalhoEhIgualAResidencia = (TipoTrabalhoIgualResidencia)props.GetInt(TRAB1_RE);
            RealizaTrabalhoExternoPrimeiroTrabalho = props.GetInt(TRABEXT1) == 1;
            OcupacaoPrimeiroTrabalho = (TipoOcupacao)props.GetInt(OCUP1);
            SetorAtividadePrimeiroTrabalho = (TipoSetorAtividade)props.GetInt(SETOR1);
            VinculoEmpregaticioPrimeiroTrabalho = (TipoVinculoEmpregaticio)props.GetInt(VINC1);
            ZonaSegundoTrabalho = props.GetInt(ZONATRA2);
            MunicipioSegundoTrabalho = props.GetInt(MUNITRA2);
            CoordenadaXSegundoTrabalho = props.GetInt(CO_TR2_X);
            CoordenadaYSegundoTrabalho = props.GetInt(CO_TR2_Y);
            SegundoTrabalhoEhIgualAResidencia = (TipoTrabalhoIgualResidencia)props.GetInt(TRAB2_RE);
            RealizaTrabalhoExternoSegundoTrabalho = props.GetInt(TRABEXT2) == 1;

            OcupacaoSegundoTrabalho = (TipoOcupacao)props.GetInt(OCUP2);
            SetorAtividadeSegundoTrabalho = (TipoSetorAtividade)props.GetInt(SETOR2);
            VinculoEmpregaticioSegundoTrabalho = (TipoVinculoEmpregaticio)props.GetInt(VINC2);
            NumeroDaViagem = props.GetInt(N_VIAG);
            FatorDeExpansaoDaViagem = props.GetDouble(FE_VIA);
            DiaDaSemana = (TipoDiaDaSemana)props.GetInt(DIA_SEM);
            TotalViagensDaPessoa = props.GetInt(TOT_VIAG);
            ZonaDeOrigem = props.GetInt(ZONA_O);
            MunicipioOrigem = props.GetInt(MUNI_O);
            CoordenadaXOrigem = props.GetInt(CO_O_X);
            CoordenadaYOrigem = props.GetInt(CO_O_Y);
            ZonaDeDestino = props.GetInt(ZONA_D);
            MunicipioDeDestino = props.GetInt(MUNI_D);
            CoordenadaXDestino = props.GetInt(CO_D_X);
            CoordenadaYDestino = props.GetInt(CO_D_Y);
            ZonaPrimeiraTransferencia = props.GetInt(ZONA_T1);
            MunicipioPrimeiraTransferencia = props.GetInt(MUNI_T1);
            CoordenadaXPrimeiraTransferencia = props.GetInt(CO_T1_X);
            CoordenadaYPrimeiraTransferencia = props.GetInt(CO_T1_Y);
            
            ZonaSegundaTransferencia = props.GetInt(ZONA_T2);
            MunicipioSegundaTransferencia = props.GetInt(MUNI_T2);
            CoordenadaXSegundaTransferencia = props.GetInt(CO_T2_X);
            CoordenadaYSegundaTransferencia = props.GetInt(CO_T2_Y);

            ZonaTerceiraTransferencia = props.GetInt(ZONA_T3);
            MunicipioTerceiraTransferencia = props.GetInt(MUNI_T3);
            CoordenadaXTerceiraTransferencia = props.GetInt(CO_T3_X);
            CoordenadaYTerceiraTransferencia = props.GetInt(CO_T3_Y);

            MotivoNaOrigem = (TipoMotivo)props.GetInt(MOTIVO_O);
            MotivoNoDestino = (TipoMotivo)props.GetInt(MOTIVO_D);

            ServirPassageiroNaOrigem = props.GetInt(SERVIR_O) == 1;
            ServiroPassageiroNoDestino = props.GetInt(SERVIR_D) == 1;

            Modo1 = (TipoModoViagem)props.GetInt(MODO1);
            Modo2 = (TipoModoViagem)props.GetInt(MODO2);
            Modo3 = (TipoModoViagem)props.GetInt(MODO3);
            Modo4 = (TipoModoViagem)props.GetInt(MODO4);

            HoraSaida = props.GetInt(H_SAIDA);
            MinutoSaida = props.GetInt(MIN_SAIDA);
            TempoAndandoNaOrigem = props.GetInt(ANDA_O);

            HoraChegada = props.GetInt(H_CHEG);
            MinutoChegada = props.GetInt(MIN_CHEG);
            TempoAndandoNoDestino = props.GetInt(ANDA_D);

            DuracaoDaViagem = props.GetInt(DURACAO);
            ModoPrincipal = (TipoModoViagem)props.GetInt(MODOPRIN);
            TipoDeViagem = (TipoViagem)props.GetInt(TIPOVG);
            PagadorViagem = (TipoPagador)props.GetInt(PAG_VIAG);
            TipoEstacionamentoAutomovel = (TipoEstacionamento)props.GetInt(TP_ESAUTO);

            ValorEstacionamentoAutomovel = props.GetDouble(VL_EST);
            MotivoViagemApeOuBicicleta = (TipoMotivoViagem)props.GetInt(PE_BICI);
            UsouViaSegregada = props.GetInt(VIA_BICI) == 1;
            TipoEstacionamentoBicicleta = (Enum.TipoEstacionamentoBicicleta)props.GetInt(TP_ESBICI);
            IdOrdem = props.GetInt(ID_ORDEM);
        }

        #region Constantes
        private const int ZONA = 1;

        private const int MUNI_DOM = 2;

        private const int CO_DOM_X = 3;

        private const int CO_DOM_Y = 4;

        private const int ID_DOM = 5;

        private const int F_DOM = 6;

        private const int FE_DOM = 7;

        private const int DOM = 8;

        private const int CD_ENTRE = 9;

        private const int DATA = 10;

        private const int TIPO_DOM = 11;

        private const int AGUA = 12;

        private const int RUA_PAVI = 13;

        private const int NO_MORAD = 14;

        private const int TOT_FAM = 15;

        private const int ID_FAM = 16;

        private const int F_FAM = 17;

        private const int FE_FAM = 18;

        private const int FAMILIA = 19;

        private const int NO_MORAF = 20;

        private const int CONDMORA = 21;

        private const int QT_BANHO = 22;

        private const int QT_EMPRE = 23;

        private const int QT_AUTO = 24;

        private const int QT_MICRO = 25;

        private const int QT_LAVALOU = 26;

        private const int QT_GEL1 = 27;

        private const int QT_GEL2 = 28;

        private const int QT_FREEZ = 29;

        private const int QT_MLAVA = 30;

        private const int QT_DVD = 31;

        private const int QT_MICROON = 32;

        private const int QT_MOTO = 33;

        private const int QT_SECAROU = 34;

        private const int QT_BICICLE = 35;

        private const int NAO_DCL_IT = 36;

        private const int CRITERIOBR = 37;

        private const int PONTO_BR = 38;

        private const int ANO_AUTO1 = 39;

        private const int ANO_AUTO2 = 40;

        private const int ANO_AUTO3 = 41;

        private const int RENDA_FA = 42;

        private const int CD_RENFA = 43;

        private const int ID_PESS = 44;

        private const int F_PESS = 45;

        private const int FE_PESS = 46;

        private const int PESSOA = 47;

        private const int SIT_FAM = 48;

        private const int IDADE = 49;

        private const int SEXO = 50;

        private const int ESTUDA = 51;

        private const int GRAU_INS = 52;

        private const int CD_ATIVI = 53;

        private const int CO_REN_I = 54;

        private const int VL_REN_I = 55;

        private const int ZONA_ESC = 56;

        private const int MUNIESC = 57;

        private const int CO_ESC_X = 58;

        private const int CO_ESC_Y = 59;

        private const int TIPO_ESC = 60;

        private const int ZONATRA1 = 61;

        private const int MUNITRA1 = 62;

        private const int CO_TR1_X = 63;

        private const int CO_TR1_Y = 64;

        private const int TRAB1_RE = 65;

        private const int TRABEXT1 = 66;

        private const int OCUP1 = 67;

        private const int SETOR1 = 68;

        private const int VINC1 = 69;

        private const int ZONATRA2 = 70;

        private const int MUNITRA2 = 71;

        private const int CO_TR2_X = 72;

        private const int CO_TR2_Y = 73;

        private const int TRAB2_RE = 74;

        private const int TRABEXT2 = 75;

        private const int OCUP2 = 76;

        private const int SETOR2 = 77;

        private const int VINC2 = 78;

        private const int N_VIAG = 79;

        private const int FE_VIA = 80;

        private const int DIA_SEM = 81;

        private const int TOT_VIAG = 82;

        private const int ZONA_O = 83;

        private const int MUNI_O = 84;

        private const int CO_O_X = 85;

        private const int CO_O_Y = 86;

        private const int ZONA_D = 87;

        private const int MUNI_D = 88;

        private const int CO_D_X = 89;

        private const int CO_D_Y = 90;

        private const int ZONA_T1 = 91;

        private const int MUNI_T1 = 92;

        private const int CO_T1_X = 93;

        private const int CO_T1_Y = 94;

        private const int ZONA_T2 = 95;

        private const int MUNI_T2 = 96;

        private const int CO_T2_X = 97;

        private const int CO_T2_Y = 98;

        private const int ZONA_T3 = 99;

        private const int MUNI_T3 = 100;

        private const int CO_T3_X = 101;

        private const int CO_T3_Y = 102;

        private const int MOTIVO_O = 103;

        private const int MOTIVO_D = 104;

        private const int SERVIR_O = 105;

        private const int SERVIR_D = 106;

        private const int MODO1 = 107;

        private const int MODO2 = 108;

        private const int MODO3 = 109;

        private const int MODO4 = 110;

        private const int H_SAIDA = 111;

        private const int MIN_SAIDA = 112;

        private const int ANDA_O = 113;

        private const int H_CHEG = 114;

        private const int MIN_CHEG = 115;

        private const int ANDA_D = 116;

        private const int DURACAO = 117;

        private const int MODOPRIN = 118;

        private const int TIPOVG = 119;

        private const int PAG_VIAG = 120;

        private const int TP_ESAUTO = 121;

        private const int VL_EST = 122;

        private const int PE_BICI = 123;

        private const int VIA_BICI = 124;

        private const int TP_ESBICI = 125;

        private const int ID_ORDEM = 126;

        #endregion

        #region Propriedades

        public int ZonaDoDomicilio { get; set; }

        public int MunicipioDomicilio { get; set; }

        public int CoordenadaXDomicilio { get; set; }

        public int CoordenadaYDomicilio { get; set; }

        public int IdDomicilio { get; set; }

        public bool EhPrimeiroRegistroDomicilio { get; set; }

        public double FatorDeExpansaoDoDomicilio { get; set; }

        public int NumeroDomicilio { get; set; }

        public TipoEntrevista CodEntrevista { get; set; }

        public DateTime DataDaEntrevista { get; set; }

        public TipoDomicilio TipoDomicilio { get; set; }

        public bool PossuiAguaEncanada { get; set; }

        public bool RuaPavimentada { get; set; }

        public int TotalDeMoradoresNoDomicilio { get; set; }

        public int TotalDeFamiliasNoDomicilio { get; set; }

        public int IdFamilia { get; set; }

        public bool EhPrimeiroRegistroFamilia { get; set; }

        public double FatorDeExpansaoDaFamilia { get; set; }

        public int NumeroDaFamilia { get; set; }

        public int TotalDeMoradoresNaFamilia { get; set; }

        public TipoMoradia CondicaoMoradia { get; set; }

        public int QtdBanheiros { get; set; }

        public int QtdEmpregadosDomesticos { get; set; }

        public int QtdAutomoveis { get; set; }

        public int QtdMicrocomputadores { get; set; }

        public int QtdMaquinasDeLavarLouca { get; set; }

        public int QtdGeladeirasDe1Porta { get; set; }

        public int QtdGeladeirasDe2Portas { get; set; }

        public int QtdFreezer { get; set; }

        public int QtdMaquinasDeLavar { get; set; }

        public int QtdDvds { get; set; }

        public int QtdMicroondas { get; set; }

        public int QtdMotos { get; set; }

        public int QtdSecadoraDeRoupas { get; set; }

        public int QtdBicicletas { get; set; }

        public bool DeclarouItensDeConforto { get; set; }

        public TipoClassificacaoEconomica CriterioDeClassificacaoEconomica { get; set; }

        public int PontosCriterioBrasil { get; set; }

        public int AnoFabricacaoAuto1 { get; set; }

        public int AnoFabricacaoAuto2 { get; set; }

        public int AnoFabricacaoAuto3 { get; set; }

        public double RendaFamiliarMensal { get; set; }

        public TipoRendaFamiliar CodigoDeRendaFamiliar { get; set; }

        public long IdPessoa { get; set; }

        public bool EhPrimeiroRegistroPessoa { get; set; }

        public double FatorExpansaoPessoa { get; set; }

        public int NumeroDaPessoa { get; set; }

        public TipoSituacaoFamiliar SituacaoFamiliar { get; set; }

        public int Idade { get; set; }

        public TipoGenero Genero { get; set; }

        public TipoSituacaoEstudo EstudaAtualmente { get; set; }

        public TipoGrauInstrucao GrauInstrucao { get; set; }

        public TipoAtividade CondicaoDeAtividade { get; set; }

        public TipoComposicaoRenda CondicaoDeRendaIndividual { get; set; }

        public double RendaIndividual { get; private set; }

        public int ZonaDaEscola { get; set; }

        public int MunicipioDaEscola { get; private set; }

        public int CoordenadaXEscola { get; set; }

        public int CoordenadaYEscola { get; set; }

        public TipoEscola TipoEscola { get; set; }

        public int ZonaPrimeiroTrabalho { get; set; }

        public int MunicipioPrimeiroTrabalho { get; set; }

        public int CoordenadaXPrimeiroTrabalho { get; set; }

        public int CoordenadaYPrimeiroTrabalho { get; set; }

        public TipoTrabalhoIgualResidencia PrimeiroTrabalhoEhIgualAResidencia { get; set; }

        /// <summary>
        /// 1 sim, 2 não
        /// </summary>
        public bool RealizaTrabalhoExternoPrimeiroTrabalho { get; set; }

        public TipoOcupacao OcupacaoPrimeiroTrabalho { get; set; }

        public TipoSetorAtividade SetorAtividadePrimeiroTrabalho { get; set; }

        public TipoVinculoEmpregaticio VinculoEmpregaticioPrimeiroTrabalho { get; set; }

        public int ZonaSegundoTrabalho { get; set; }

        public int MunicipioSegundoTrabalho { get; set; }

        public int CoordenadaXSegundoTrabalho { get; set; }

        public int CoordenadaYSegundoTrabalho { get; set; }

        public TipoTrabalhoIgualResidencia SegundoTrabalhoEhIgualAResidencia { get; set; }

        public bool RealizaTrabalhoExternoSegundoTrabalho { get; set; }

        public TipoOcupacao OcupacaoSegundoTrabalho { get; set; }

        public TipoSetorAtividade SetorAtividadeSegundoTrabalho { get; set; }

        public TipoVinculoEmpregaticio VinculoEmpregaticioSegundoTrabalho { get; set; }

        public int NumeroDaViagem { get; set; }

        public double FatorDeExpansaoDaViagem { get; set; }

        public TipoDiaDaSemana DiaDaSemana { get; set; }

        public int TotalViagensDaPessoa { get; set; }

        public int ZonaDeOrigem { get; set; }

        public int MunicipioOrigem { get; set; }

        public int CoordenadaXOrigem { get; set; }

        public int CoordenadaYOrigem { get; set; }

        public int ZonaDeDestino { get; set; }

        public int MunicipioDeDestino { get; set; }

        public int CoordenadaXDestino { get; set; }

        public int CoordenadaYDestino { get; set; }

        public int ZonaPrimeiraTransferencia { get; set; }

        public int MunicipioPrimeiraTransferencia { get; set; }

        public int CoordenadaXPrimeiraTransferencia { get; set; }

        public int CoordenadaYPrimeiraTransferencia { get; set; }

        public int ZonaSegundaTransferencia { get; set; }

        public int MunicipioSegundaTransferencia { get; set; }

        public int CoordenadaXSegundaTransferencia { get; set; }

        public int CoordenadaYSegundaTransferencia { get; set; }

        public int ZonaTerceiraTransferencia { get; set; }

        public int MunicipioTerceiraTransferencia { get; set; }

        public int CoordenadaXTerceiraTransferencia { get; set; }

        public int CoordenadaYTerceiraTransferencia { get; set; }

        public TipoMotivo MotivoNaOrigem { get; set; }

        public TipoMotivo MotivoNoDestino { get; set; }

        public bool ServirPassageiroNaOrigem { get; set; }

        public bool ServiroPassageiroNoDestino { get; set; }

        public TipoModoViagem Modo1 { get; set; }

        public TipoModoViagem Modo2 { get; set; }

        public TipoModoViagem Modo3 { get; set; }

        public TipoModoViagem Modo4 { get; set; }

        public int HoraSaida { get; set; }

        public int MinutoSaida { get; set; }

        /// <summary>
        /// Tempo em minutos gastos andando na origem
        /// </summary>
        public int TempoAndandoNaOrigem { get; set; }

        public int HoraChegada { get; set; }

        public int MinutoChegada { get; set; }

        /// <summary>
        /// Tempo em minutos gastos andando no destino
        /// </summary>
        public int TempoAndandoNoDestino { get; set; }

        /// <summary>
        /// Duracao da viagem em minutos
        /// </summary>
        public int DuracaoDaViagem { get; set; }

        public TipoModoViagem ModoPrincipal { get; set; }

        public TipoViagem TipoDeViagem { get; set; }

        public TipoPagador PagadorViagem { get; set; }

        public TipoEstacionamento TipoEstacionamentoAutomovel { get; set; }

        public double ValorEstacionamentoAutomovel { get; set; }

        public TipoMotivoViagem MotivoViagemApeOuBicicleta { get; set; }

        /// <summary>
        /// Caso tenha viajado de bicicleta
        /// </summary>
        public bool UsouViaSegregada { get; set; }

        public TipoEstacionamentoBicicleta TipoEstacionamentoBicicleta { get; set; }

        public int IdOrdem { get; set; }

        #endregion
    }
}
