﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ACH2024.GrafoOrigemDestino.Api.Models
{
    public class Local
    {
        public Local()
        {
            Frequentadores = new List<long>();
        }

        public int Zona { get; set; }

        public int Municipio { get; set; }

        public int CoordenadaX { get; set; }

        public int CoordenadaY { get; set; }

        public List<long> Frequentadores { get; set; }

        public int TotalFrequentadores { get => Frequentadores.Count; }

    }
}
