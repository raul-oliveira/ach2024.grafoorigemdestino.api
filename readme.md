Repositório para as atividades de grafo da disciplina de AED II (ACH 2024)
Nome: Raul Soares de Oliveira
NUSP: 9878016

Instruções: 
    Colocar a base da pesquisa de origem e destino no mesmo diretório do executável ou na pasta C:/Temp/OD_2017.csv

Args: 
    -f: o programa assumira que já existe um arquivo com os locais processados

OBS:
    Os algoritmos serão otimizados no decorrer das tarefas, se houver necessidade.
    Se estiver com dificuldades para abrir o histograma no index.html, me comunique via e-mail: mrraul65@gmail.com
     